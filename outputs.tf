output "tags" {
  value       = data.aws_ami.ami.*.tags
  description = "List of maps of tags for each AMI found, can be used to produce shared_tags input"
}