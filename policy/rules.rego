package main

# Accessors

data_ami := data("aws_ami", "ami")

resource_sharing := resource("aws_ami_launch_permission", "sharing")

resource_tags := resource("null_resource", "tags")

output_tags := output("tags")

provider_source := provider("aws", "source")

provider_destination := provider("aws", "destination")

data_identity_source := data("aws_caller_identity", "source")

data_identity_destination := data("aws_caller_identity", "destination")

variable_ami_ids := variable("ami_ids")

variable_profile := variable("profile")

variable_shared_tags := variable("shared_tags")

local_tags := local("tags")

# Rules

deny[msg] {
  count(data_ami) != 1
  msg = "Define the ami data source"
}

deny[msg] {
  count([value | value := data_ami[_].provider; value == "${aws.source}"]) != 1
  msg = "The ami data source must use the source provider"
}

deny[msg] {
  count(resource_sharing) != 1
  msg = "Define the sharing launch permission resource"
}

deny[msg] {
  count([value | value := resource_sharing[_].provider; value == "${aws.source}"]) != 1
  msg = "The sharing launch permission resource must us the source provider"
}

deny[msg] {
  count(resource_tags) != 1
  msg = "Define the tags null resource"
}

deny[msg] {
  count(output_tags) != 1
  msg = "Define the tags output"
}

deny[msg] {
  count([value | value := output_tags[_].value; value == "${data.aws_ami.ami.*.tags}"]) != 1
  msg = "The tags output must the tags of the ami found by the data source"
}

deny[msg] {
  count(provider_source) != 1
  msg = "Define the source provider"
}

deny[msg] {
  count(provider_source[0]) != 1
  msg = "The source provider must be a proxy configuration block"
}

deny[msg] {
  count(provider_destination) != 1
  msg = "Define the destination provider"
}

deny[msg] {
  count(provider_destination[0]) != 1
  msg = "The destination provider must be a proxy configuration block"
}

deny[msg] {
  count(data_identity_source) != 1
  msg = "Define the source identity"
}

deny[msg] {
  count([value | value := data_identity_source[_].provider; value == "${aws.source}"]) != 1
  msg = "The source identity must use the source provider"
}

deny[msg] {
  count(data_identity_destination) != 1
  msg = "Define the destination identity"
}

deny[msg] {
  count([value | value := data_identity_destination[_].provider; value == "${aws.destination}"]) != 1
  msg = "The destination identity must use the destination provider"
}

deny[msg] {
  count([value | value := data_ami[_].count; value == "${length(var.ami_ids)}"]) != 1
  msg = "The ami data source must iterate over the ami_ids variable"
}

deny[msg] {
  count([value | value := resource_sharing[_].count; value == "${length(var.ami_ids)}"]) != 1
  msg = "The sharing launch permission resource must iterate over the ami_ids variable"
}

deny[msg] {
  count([value | value := resource_tags[_].count; value == "${length(var.ami_ids)}"]) != 1
  msg = "The tags null resource must iterate over the ami_ids variable"
}

deny[msg] {
  count(variable_ami_ids) != 1
  msg = "Define the ami_ids variable"
}

deny[msg] {
  count(variable_profile) != 1
  msg = "Define the profile variable"
}

deny[msg] {
  count(variable_shared_tags) != 1
  msg = "Define the shared_tags variable"
}

deny[msg] {
  count([value | value := resource_tags[_].triggers.resource_id; value == "${element(var.ami_ids, count.index)}"]) != 1
  msg = "The tags resource must use ami_ids variable iterator as the resource_id trigger"
}

deny[msg] {
  count([value | value := resource_tags[_].triggers.tags; value == "${element(local.tags, count.index)}"]) != 1
  msg = "The tags resource must use tags local iterator as the tags trigger"
}

deny[msg] {
  count([value | value := resource_tags[_].triggers.profile; value == "${var.profile}"]) != 1
  msg = "The tags resource must use the profile variable as the profile trigger"
}

deny[msg] {
  count(local_tags) != 1
  msg = "Define the tags local"
}

deny[msg] {
  count([value | value := resource_sharing[_].account_id; value == "${data.aws_caller_identity.destination.account_id}"]) != 1
  msg = "The sharing launch permission resource must use the destination identity for its account_id"
}

deny[msg] {
  count([value | value := resource_sharing[_].image_id; value == "${element(var.ami_ids, count.index)}"]) != 1
  msg = "The sharing launch permission resource must use ami_ids variable iterator as its image_id"
}