# Share AMIS with tags module
This module provides a way to share some AMIs with another account and copy tags (merging them with an input additional map) to the shared AMIs.    
It uses Terraform >= 0.12 for structure.
